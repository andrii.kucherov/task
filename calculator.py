
def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def divide(x, y):
    return x / y


while True:
    num1 = float(input("Enter first number: "))
    action = input("Enter action: ")
    num2 = float(input("Enter second number: "))

    if action == '+':
        print(num1, "+", num2, "=", add(num1, num2))

    elif action == '-':
        print(num1, "-", num2, "=", subtract(num1, num2))

    elif action == '*':
        print(num1, "*", num2, "=", multiply(num1, num2))

    elif action == '/':
        print(num1, "/", num2, "=", divide(num1, num2))
        
    next_calculation = input("Let's do next calculation? (yes/no): ")
    if (next_calculation == "no" or next_calculation =="n"):
        break
    
