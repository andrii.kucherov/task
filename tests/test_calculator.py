import pytest
from calculator import add

class TestAdd():
    def test_positive_value(self):
        assert add(3,4) == 7

    def test_negative_value(self):
        assert add(3,-4) == -1

    def test_zero_value(self):
        assert add(0,4) == 4

    def test_value(self):
        result = add(0,4.5)
        assert type(result) is float
