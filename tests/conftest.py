import pytest
from student_class import *

@pytest.fixture()
def some_number():
    return 42

@pytest.fixture()
def num_arr():
    return [2,4,6,8,9]

@pytest.fixture(scope='module')
def db():
    db = marksDB()
    db.connect('tests\data.json')
    yield db
    db.close()