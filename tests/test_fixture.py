import pytest
import sys
from student_class import marksDB

def multiple(x,y):
    return x*y

def test_some_data(some_number):
    assert some_number == 42

def test_pair_number(num_arr): 
    assert (i for i in num_arr if i%2==0)

@pytest.mark.skipif(sys.version_info > (3, 3), reason="Test Skiping")
def test_add_returns_valid_id():
    assert True

@pytest.mark.parametrize('x,y,result', [(5,4,20),(5.5,2,11)])
def test_multip(x, y, result):
    assert multiple(x,y) == result

def test_Oleg_data(db):
    Oleg_data = db.get_data('Oleg')
    assert Oleg_data['id'] == 1
    assert Oleg_data['name'] == "Oleg"
    assert Oleg_data['mark'] == 10